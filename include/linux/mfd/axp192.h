/*
 *  include/linux/mfd/axp192.h
 *  Driver for X-Power AXP192 PMIC
 *
 *  ( based on 
 *    https://github.com/cnxsoft/telechips-linux/blob/master/drivers/regulator/axp192.c
 *    modified for AXP192 on T-station)
 *
 *  Copyright (C) 2019 T-Station test
 *  T-Station test <tstation@tstation-test.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

#ifndef __LINUX_MFD_AXP192_H
#define __LINUX_MFD_AXP192_H

#include <linux/power_supply.h>
#include <linux/alarmtimer.h>
#include <linux/wakelock.h>



//**************************************
// constant definition
//**************************************
#define AXP192_DEV_NAME   "axp192"
#define AXP192_DRV_VER	   "1.0"


// compatible string used in
//    mfd/axp192-core.c
//    mfd/axp192-irq.c
//    gpio/gpio-axp192.c
//    regulator/axp192-regulator.c
#define AXP192_OF_COMATIBLE(suffix) "x-powers," AXP192_DEV_NAME "-" #suffix


// regulator
enum AXP192_REGULATOR_ID {
	AXP192_ID_DCDC1 = 0,
	AXP192_ID_DCDC2,
	AXP192_ID_DCDC3,
	AXP192_ID_LDO2,
	AXP192_ID_LDO3,
	AXP192_ID_LDO4,

	AXP192_ID_DUMMY,  // this entry must be last in the enum.
  
	AXP192_ID_LAST = AXP192_ID_DUMMY,
};

//**************************************
// core
//**************************************
struct axp192_core {
  struct i2c_client*  i2c;
  struct device*      dev;
	struct regmap*      regmap; // freed by device management code
};



//**************************************
//	I2C Command & Values
//**************************************
//--------------------------------------
// bus access
//--------------------------------------
#define AXP192_READ(regmap,reg,val) regmap_read(regmap,reg,val)
#define AXP192_WRITE(regmap,reg,val) regmap_write(regmap,reg,val)
#define AXP192_WRITE_BITS(regmap,reg,mask,val) regmap_update_bits(regmap,reg,mask,val)


//--------------------------------------
// Registers
//--------------------------------------
#define AXP192_POWER_STS_REG            0x00
#define AXP192_MODE_CHARGING_STS_REG    0x01
#define AXP192_OTG_VBUS_STS_REG         0x04
#define AXP192_EXTEN_DCDC2_CTRL_REG     0x10
#define AXP192_DCDC_LDO23_CTRL_REG      0x12
#define AXP192_DCDC2_REG                0x23

#define AXP192_DCDC2_VRC_REG            0x25

#define AXP192_DCDC1_REG                0x26
#define AXP192_DCDC3_REG                0x27
#define AXP192_LDO23_REG                0x28
#define AXP192_VBUS_IPSOUT_REG          0x30
#define AXP192_POWER_OFF_VOL_REG        0x31
#define AXP192_POWER_OFF_REG            0x32
#define AXP192_CHARGING_CTRL1_REG       0x33
#define AXP192_CHARGING_CTRL2_REG       0x34
#define AXP192_BACKUP_BATT_CHG_REG      0x35
#define AXP192_PEK_REG                  0x36

// irq
#define AXP192_IRQ_EN1_REG              0x40
#define AXP192_IRQ_EN2_REG              0x41
#define AXP192_IRQ_EN3_REG              0x42
#define AXP192_IRQ_EN4_REG              0x43
#define AXP192_IRQ_STS1_REG             0x44
#define AXP192_IRQ_STS2_REG             0x45
#define AXP192_IRQ_STS3_REG             0x46
#define AXP192_IRQ_STS4_REG             0x47

// adc
#define AXP192_ADC_ACIN_VOL_H_REG       0x56
#define AXP192_ADC_ACIN_VOL_L_REG       0x57
#define AXP192_ADC_VBUS_VOL_H_REG       0x5A
#define AXP192_ADC_VBUS_VOL_L_REG       0x5B
#define AXP192_ADC_BATT_VOL_H_REG       0x78
#define AXP192_ADC_BATT_VOL_L_REG       0x79

// BAT Charge
#define AXP192_BATT_CHARGE_CURRENT_H_REG 0x7A
#define AXP192_BATT_CHARGE_CURRENT_L_REG 0x7B
#define AXP192_BATT_DISCHARGE_CURRENT_H_REG 0x7C
#define AXP192_BATT_DISCHARGE_CURRENT_L_REG 0x7D

#define AXP192_DCDC123_MODE_REG         0x80

// adc
#define AXP192_ADC_ENABLE1_REG          0x82
#define AXP192_ADC_ENABLE2_REG          0x83
#define AXP192_ADC_TSPIN_REG            0x84

#define AXP192_OVER_TEMP_REG            0x8F
#define AXP192_GPIO0_FUNC_REG           0x90
#define AXP192_LDO4_VOLTAGE_REG         0x91

// gpio
#define AXP192_GPIO1_FUNC_SET_REG       0x92
#define AXP192_GPIO2_FUNC_SET_REG       0x93
#define AXP192_GPIO012_STATUS_REG       0x94
#define AXP192_GPIO34_FUNC_SET_REG      0x95
#define AXP192_GPIO34_STATUS_REG        0x96


//--------------------------------------
// regsiter values
//--------------------------------------
// DCDC[1,2,3], LDO[2,3] control (AXP192_DCDC_LDO23_CTRL_REG 0x12)
#define AXP192_DCDC_LDO23_CTRL_RESERVE1 0x80
#define AXP192_DCDC_LDO23_CTRL_EXTEN    0x40
#define AXP192_DCDC_LDO23_CTRL_RESERVE2 0x20
#define AXP192_DCDC_LDO23_CTRL_DCDC2    0x10
#define AXP192_DCDC_LDO23_CTRL_LDO3     0x08
#define AXP192_DCDC_LDO23_CTRL_LDO2     0x04
#define AXP192_DCDC_LDO23_CTRL_DCDC3    0x02
#define AXP192_DCDC_LDO23_CTRL_DCDC1    0x01

// DC-DC2 dynamic voltage regulation parameter setting values (AXP192_DCDC2_VRC_REG 0x25) 
#define AXP192_DCDC2_VRC_ENABLE         0x04
#define AXP192_DCDC2_VRC_SLOPE_HIGH     0x00  // 25mV/15.625us == 1.6mV/us
#define AXP192_DCDC2_VRC_SLOPE_LOW      0x01  // 25mV/31.250us == 0.8mV/us
//... return in 1000uSec, step==25mV
#define AXP192_DCDC2_VRC_time_per_step(slope_hi_or_lo)   (15625<<slope_hi_or_lo)


// VBUS-IPSOUT Path Management values (AXP192_VBUS_IPSOUT_REG 0x30)
#define AXP192_VBUS_IPSOUT_BY_N_VBUSEN  0x00
#define AXP192_VBUS_IPSOUT_ALWAYS_ON    0x80
#define AXP192_VBUS_VHOLD_LIMIT_EN      0x40
#define AXP192_VBUS_VHOLD_VOL_4_0       0x00
#define AXP192_VBUS_VHOLD_VOL_4_1       0x08
#define AXP192_VBUS_VHOLD_VOL_4_2       0x10
#define AXP192_VBUS_VHOLD_VOL_4_3       0x18
#define AXP192_VBUS_VHOLD_VOL_4_4       0x20
#define AXP192_VBUS_VHOLD_VOL_4_5       0x28
#define AXP192_VBUS_VHOLD_VOL_4_6       0x30
#define AXP192_VBUS_VHOLD_VOL_4_7       0x38
#define AXP192_VBUS_CURRENT_LIMIT_EN    0x02
#define AXP192_VBUS_CURRENT_LIMIT_500   0x00
#define AXP192_VBUS_CURRENT_LIMIT_100   0x01


// Power off voltage values (AXP192_POWER_OFF_VOL_REG 0x31)
#define AXP192_PWROFF_VOL_2_6V          0x00
#define AXP192_PWROFF_VOL_2_7V          0x01
#define AXP192_PWROFF_VOL_2_8V          0x02
#define AXP192_PWROFF_VOL_2_9V          0x03
#define AXP192_PWROFF_VOL_3_0V          0x04
#define AXP192_PWROFF_VOL_3_1V          0x05
#define AXP192_PWROFF_VOL_3_2V          0x06
#define AXP192_PWROFF_VOL_3_3V          0x07
#define AXP192_PWROFF_VOL_MASK          0x07

// Power off values (AXP192_POWER_OFF_REG 0x32)
#define AXP192_PWROFF_EN                0x80
#define AXP192_PWROFF_BATT_MON_EN       0x40
#define AXP192_PWROFF_BATT_MON_DIS      0x00
#define AXP192_PWROFF_CHGLED_HIGHZ      0x00
#define AXP192_PWROFF_CHGLED_1Hz        0x10
#define AXP192_PWROFF_CHGLED_4Hz        0x20
#define AXP192_PWROFF_CHGLED_OUTLOW     0x30
#define AXP192_PWROFF_CHGLED_MASK       0x30
#define AXP192_PWROFF_CHGLED_CTRL_CHG   0x00
#define AXP192_PWROFF_CHGLED_CTRL_REG   0x08
#define AXP192_PWROFF_RESERVED          0x04
#define AXP192_PWROFF_N_OE_DLY_0_5S     0x00
#define AXP192_PWROFF_N_OE_DLY_1_0S     0x01
#define AXP192_PWROFF_N_OE_DLY_2_0S     0x02
#define AXP192_PWROFF_N_OE_DLY_3_0S     0x03
#define AXP192_PWROFF_N_OE_DLY_MASK     0x03

// Changing control 1 values (AXP192_CHARGING_CTRL1_REG 0x33)
#define AXP192_CHG_EN                   0x80
#define AXP192_CHG_VOL_4_10V            0x00
#define AXP192_CHG_VOL_4_15V            0x20
#define AXP192_CHG_VOL_4_20V            0x40
#define AXP192_CHG_VOL_4_36V            0x60
#define AXP192_CHG_VOL_MASK             0x60
#define AXP192_CHG_OFF_CUR_10PER        0x00
#define AXP192_CHG_OFF_CUR_15PER        0x10
#define AXP192_CHG_OFF_CUR_MASK         0x10

// PEK press key parameter set vaules (AXP192_PEK_REG 0x36)
#define AXP192_PEK_PWRON_128mS          0x00
#define AXP192_PEK_PWRON_256mS          0x40
#define AXP192_PEK_PWRON_512mS          0x80
#define AXP192_PEK_PWRON_1S             0xC0
#define AXP192_PEK_PWRON_MASK           0xC0
#define AXP192_PEK_LONGKEY_1_0S         0x00
#define AXP192_PEK_LONGKEY_1_5S         0x10
#define AXP192_PEK_LONGKEY_2_0S         0x20
#define AXP192_PEK_LONGKEY_2_5S         0x30
#define AXP192_PEK_LONGKEY_MASK         0x30
#define AXP192_PEK_LONGKEY_PWROFF_EN    0x08
#define AXP192_PEK_LONGKEY_PWROFF_DIS   0x00
#define AXP192_PEK_PWRON_DLY_32mS       0x00
#define AXP192_PEK_PWRON_DLY_64mS       0x04
#define AXP192_PEK_PWRON_DLY_MASK       0x04
#define AXP192_PEK_PWROFF_TIME_4S       0x00
#define AXP192_PEK_PWROFF_TIME_6S       0x01
#define AXP192_PEK_PWROFF_TIME_8S       0x02
#define AXP192_PEK_PWROFF_TIME_12S      0x03
#define AXP192_PEK_PWROFF_TIME_MASK     0x03

// Interrupt mask and status
//...IRQ 1..7 (AXP192_IRQ_EN1_REG 0x40, AXP192_IRQ_STS1_REG 0x44)
#define AXP192_IRQ_1_ACIN_OVER          0x80  // irq 1
#define AXP192_IRQ_1_ACIN_INSERT        0x40
#define AXP192_IRQ_1_ACIN_REMOVE        0x20
#define AXP192_IRQ_1_VBUS_OVER          0x10
#define AXP192_IRQ_1_VBUS_INSERT        0x08
#define AXP192_IRQ_1_VBUS_REMOVE        0x04
#define AXP192_IRQ_1_VBUS_LT_VHOLD      0x02  // irq 7
#define AXP192_IRQ_1_RESERVED           0x01
//...IRQ 8..15 (AXP192_IRQ_EN2_REG 0x41, AXP192_IRQ_STS2_REG 0x45)
#define AXP192_IRQ_2_BATT_INSERT        0x80  // irq 8
#define AXP192_IRQ_2_BATT_REMOVE        0x40
#define AXP192_IRQ_2_BATT_ACT           0x20
#define AXP192_IRQ_2_BATT_QUIT_ACT      0x10
#define AXP192_IRQ_2_CHARGING           0x08
#define AXP192_IRQ_2_CHARGE_FINISH      0x04
#define AXP192_IRQ_2_BATT_OVER          0x02
#define AXP192_IRQ_2_BATT_UNDER         0x01  // irq 15
//...IRQ 16..23 (AXP192_IRQ_EN3_REG 0x42, AXP192_IRQ_STS3_REG 0x46)
#define AXP192_IRQ_3_INTERNAL_OVER      0x80  // irq 16
#define AXP192_IRQ_3_NOT_ENOUGH_CURR    0x40
#define AXP192_IRQ_3_DCDC1_UNDER        0x20
#define AXP192_IRQ_3_DCDC2_UNDER        0x10
#define AXP192_IRQ_3_DCDC3_UNDER        0x08
#define AXP192_IRQ_3_RESERVED           0x04
#define AXP192_IRQ_3_SHORT_KEY          0x02
#define AXP192_IRQ_3_LONG_KEY           0x01  // irq 23
//...IRQ 24..30 (AXP192_IRQ_EN4_REG 0x43, AXP192_IRQ_STS4_REG 0x47)
#define AXP192_IRQ_4_POWER_ON           0x80  // irq 24
#define AXP192_IRQ_4_POWER_OFF          0x40
#define AXP192_IRQ_4_VBUS_VALID         0x20
#define AXP192_IRQ_4_VBUS_INVLAID       0x10
#define AXP192_IRQ_4_VBUS_SESSION_AB    0x08
#define AXP192_IRQ_4_VBUS_SESSION_END   0x04
#define AXP192_IRQ_4_RESERVED           0x02
#define AXP192_IRQ_4_UNDER_VOLTAGE      0x01  // irq 30

// DC-DC working mode (AXP192_DCDC123_MODE_REG 0x80)
#define AXP192_DCDC1_MODE_FIXED         0x08
#define AXP192_DCDC2_MODE_FIXED         0x04
#define AXP192_DCDC3_MODE_FIXED         0x02

// ADC Enable 1 values (AXP192_ADC_ENABLE1_REG 0x82)
#define AXP192_ADC_1_BATT_VOL           0x80
#define AXP192_ADC_1_BATT_CUR           0x40
#define AXP192_ADC_1_ACIN_VOL           0x20
#define AXP192_ADC_1_ACIN_CUR           0x10
#define AXP192_ADC_1_VBUS_VOL           0x08
#define AXP192_ADC_1_VBUS_CUR           0x04
#define AXP192_ADC_1_APS_VOL            0x02
#define AXP192_ADC_1_TS_PIN             0x01

// ADC Enable 2 values (AXP192_ADC_ENABLE2_REG 0x83)
#define AXP192_ADC_2_TEMP_MON           0x80
#define AXP192_ADC_2_GPIO0              0x08
#define AXP192_ADC_2_GPIO1              0x04
#define AXP192_ADC_2_GPIO2              0x02
#define AXP192_ADC_2_GPIO3              0x01

// ADC sample rate setting, TS pin control (AXP192_ADC_TSPIN_REG 0x84)
#define AXP192_ADC_TS_SAMPLE_25         0x00
#define AXP192_ADC_TS_SAMPLE_50         0x40
#define AXP192_ADC_TS_SAMPLE_100        0x80
#define AXP192_ADC_TS_SAMPLE_200        0xC0
#define AXP192_ADC_TS_SAMPLE_MASK       0xC0
#define AXP192_ADC_TS_OUT_CUR_20uA      0x00
#define AXP192_ADC_TS_OUT_CUR_40uA      0x10
#define AXP192_ADC_TS_OUT_CUR_60uA      0x20
#define AXP192_ADC_TS_OUT_CUR_80uA      0x30
#define AXP192_ADC_TS_OUT_CUR_MASK      0x30
#define AXP192_ADC_TS_PIN_TEMP_MON      0x00
#define AXP192_ADC_TS_PIN_EXTERN_ADC    0x04
#define AXP192_ADC_TS_PIN_OUT_DIS       0x00
#define AXP192_ADC_TS_PIN_OUT_CHG       0x01
#define AXP192_ADC_TS_PIN_OUT_SAVE_ENG  0x02
#define AXP192_ADC_TS_PIN_OUT_ALWAYS    0x03
#define AXP192_ADC_TS_PIN_OUT_MASK      0x03

// Over-temperature shutdown and other function settings(AXP192_OVER_TEMP_REG 0x8F)
#define AXP192_OVER_TEMP_SHUTDOWN       0x04

// GPIO0 function setting (AXP192_GPIO0_FUNC_REG 0x90)
#define AXP192_GPIO0_FUNC_OPEN_DRAIN    0x00
#define AXP192_GPIO0_FUNC_INPUT         0x01
#define AXP192_GPIO0_FUNC_LDO           0x02
#define AXP192_GPIO0_FUNC_ADC           0x04
#define AXP192_GPIO0_FUNC_LOW           0x05
#define AXP192_GPIO0_FUNC_FLOATING      0x07  // or 0x06
#define AXP192_GPIO0_FUNC_MASK          0x07

// GPIO1 function setting (AXP192_GPIO1_FUNC_SET_REG 0x92)
#define AXP192_GPIO1_FUNC_OPEN_DRAIN    0x00
#define AXP192_GPIO1_FUNC_INPUT         0x01
#define AXP192_GPIO1_FUNC_PWM1          0x02
#define AXP192_GPIO1_FUNC_RESERVED      0x03
#define AXP192_GPIO1_FUNC_ADC           0x04
#define AXP192_GPIO1_FUNC_OUT_LOW       0x05
#define AXP192_GPIO1_FUNC_FLOATING      0x07    // or 0x06
#define AXP192_GPIO1_FUNC_MASK          0x07

// GPIO2 function setting (AXP192_GPIO2_FUNC_SET_REG 0x93)
#define AXP192_GPIO2_FUNC_OPEN_DRAIN    0x00
#define AXP192_GPIO2_FUNC_INPUT         0x01
#define AXP192_GPIO2_FUNC_PWM1          0x02
#define AXP192_GPIO2_FUNC_RESERVED      0x03
#define AXP192_GPIO2_FUNC_ADC           0x04
#define AXP192_GPIO2_FUNC_OUT_LOW       0x05
#define AXP192_GPIO2_FUNC_FLOATING      0x07    // or 0x06
#define AXP192_GPIO2_FUNC_MASK          0x07

// GPIO 012 status (AXP192_GPIO012_STATUS_REG 0x94)
#define AXP192_GPIO012_STATUS_RESERVED1 0x80
#define AXP192_GPIO012_STATUS_2_IN      0x40
#define AXP192_GPIO012_STATUS_1_IN      0x20
#define AXP192_GPIO012_STATUS_0_IN      0x10
#define AXP192_GPIO012_STATUS_RESERVED2 0x08
#define AXP192_GPIO012_STATUS_2_OUT     0x04
#define AXP192_GPIO012_STATUS_1_OUT     0x02
#define AXP192_GPIO012_STATUS_0_OUT     0x01

// GPIO 3,4 function setting (AXP192_GPIO34_FUNC_SET_REG 0x95)
#define AXP192_GPIO34_FUNC_GPIO         0x80
#define AXP192_GPIO34_FUNC_4_EXTCHARGE  0x00
#define AXP192_GPIO34_FUNC_4_OPEN_DRAIN 0x04
#define AXP192_GPIO34_FUNC_4_INPUT      0x08
#define AXP192_GPIO34_FUNC_4_MASK       0x0C
#define AXP192_GPIO34_FUNC_3_EXTCHARGE  0x00
#define AXP192_GPIO34_FUNC_3_OPEN_DRAIN 0x01
#define AXP192_GPIO34_FUNC_3_INPUT      0x02
#define AXP192_GPIO34_FUNC_3_MASK       0x03

// GPIO 3,4 status (AXP192_GPIO34_STATUS_REG 0x96)
#define AXP192_GPIO34_STATUS_RESERVED1  0xC0
#define AXP192_GPIO34_STATUS_4_IN       0x20
#define AXP192_GPIO34_STATUS_3_IN       0x10
#define AXP192_GPIO34_STATUS_RESERVED2  0x0C
#define AXP192_GPIO34_STATUS_4_OUT      0x02
#define AXP192_GPIO34_STATUS_3_OUT      0x01


// battery charge
enum {
	AXP192_CHG_CURR_100mA = 0,
	AXP192_CHG_CURR_190mA,
	AXP192_CHG_CURR_280mA,
	AXP192_CHG_CURR_360mA,
	AXP192_CHG_CURR_450mA,
	AXP192_CHG_CURR_550mA,
	AXP192_CHG_CURR_630mA,
	AXP192_CHG_CURR_700mA,
	AXP192_CHG_CURR_780mA,
	AXP192_CHG_CURR_880mA,
	AXP192_CHG_CURR_960mA,
	AXP192_CHG_CURR_1000mA,
	AXP192_CHG_CURR_1080mA,
	AXP192_CHG_CURR_1160mA,
	AXP192_CHG_CURR_MAX
};

#define AXP192_CHG_OK			  0x02
#define AXP192_CHG_ING			0x01
#define AXP192_CHG_NONE			0x00



#endif /* __LINUX_MFD_AXP192_H */
