/*
 *  drivers/regulator/axp192-regulator.c
 *  ( based on rt5025-regulator.c 
 *    modified for AXP192 on T-station)
 *
 *  Driver for X-Power AXP192 PMIC(regulator part)
 *
 *  Copyright (C) 2019 T-Station test
 *  T-Station test <tstation@tstation-test.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/driver.h>
#include <linux/version.h>
#include <linux/regulator/of_regulator.h>
#include <linux/mfd/core.h>
#include <linux/regmap.h>

#include <linux/mfd/axp192.h>


//**************************************
// regulator configulation
//**************************************
// add dummy regulator(1:enable, 0:disable)
#define AXP192_DUMMY_REGULATOR      1

// use VRC (high-slope or low-slope)
//#define AXP192_DCDC2_VRC_SLOPE    AXP192_DCDC2_VRC_SLOPE_LOW  //or AXP192_DCDC2_VRC_SLOPE_HIGH
#define AXP192_DCDC2_VRC_SLOPE    AXP192_DCDC2_VRC_SLOPE_HIGH  //or AXP192_DCDC2_VRC_SLOPE_HIGH

// wait or not after setting voltage( only for dcdc2 )
#define AXP192_DCDC_WAIT_AFTER_SET  1

// enable verbose message
#define AXP192_DEBUG

//**************************************
// debug
//**************************************
#ifdef AXP192_DEBUG
#define AXP_DBG(msg...) printk("AXP192(regulator): " msg)
#else
#define AXP_DBG(msg...)
#endif
#define AXP_ERR(msg...) printk("AXP192(regulator): Error " msg)

#define AXP_UNUSED    __attribute__((unused))

//*****************************************
//  struct
//*****************************************
struct axp192_desc {
    struct regulator_desc desc;
    
    // voltage setting
    //...register
    int vol_reg;    // register
    int vol_bpos;   // bit position
    int vol_mask;   // this is alreay left-shifted by vol_bpos.
    //...selector <--> voltage mapping
    //int vol_num;  // == struct regulator_desc desc.n_voltages
    int vol_base;
    int vol_step;

    // enable/disable setting
    int en_reg;
    int en_mask;    // this is alreay left-shifted. (1:enable, 0:disable)
};

struct axp192_regulator {
    struct axp192_core*       core;
    struct axp192_desc const* axp_desc;

    // set by axp192_parse_dt()
    union {
        int             initial_uV;   // constant
        int             curr_uV;      // variable (for dummy regulator)
    };
    int                 standby_uV;   // constant
};

//**************************************
// bus access
//**************************************
static inline struct regmap* axp192_regmap( struct axp192_regulator* axp_reg )
{
    return axp_reg->core->regmap;
}

//**************************************
//  define constants
//**************************************
// DCDC
//...voltage range
#define AXP192_DCDC_BASE    700000   // uV
#define AXP192_DCDC_STEP    25000    // uV
#define AXP192_DCDC1_NUM    113
#define AXP192_DCDC2_NUM    64
#define AXP192_DCDC3_NUM    113
//#define AXP192_DCDC2_VRC_SLOPE    AXP192_DCDC2_VRC_SLOPE_LOW  //or AXP192_DCDC2_VRC_SLOPE_HIGH
#define AXP192_DCDC2_VRC_SLOPE    AXP192_DCDC2_VRC_SLOPE_HIGH  //or AXP192_DCDC2_VRC_SLOPE_HIGH
//...wait or not after setting voltage( only for dcdc2 )
#define AXP192_DCDC_WAIT_AFTER_SET  1
#if AXP192_DCDC_WAIT_AFTER_SET && !defined(AXP192_DCDC2_VRC_SLOPE)
#error  AXP192_DCDC2_VRC_SLOPE must be defined to insert wait after voltage setting.
#endif

// ldo voltage range
#define AXP192_LDO_BASE     1800000  // uV
#define AXP192_LDO_STEP     100000   // uV
#define AXP192_LDO_NUM      16

// dummy voltage range
#define AXP192_DUMMY_BASE   1000000  // uV
#define AXP192_DUMMY_STEP   100000   // uV
#define AXP192_DUMMY_NUM    16


//**************************************
//  Functions (Internal)
//**************************************
//--------------------------------------
//  regulator common
//--------------------------------------
static int axp192_common_list_voltage(struct regulator_dev* rdev, unsigned selector)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    unsigned const            vol_num = (unsigned)axp_desc->desc.n_voltages;
    int const                 vol_base = axp_desc->vol_base;
    int const                 vol_step = axp_desc->vol_step;
    int                       voltage = -EINVAL;

    if ( selector < vol_num )
        voltage = vol_base + vol_step * selector;
    else
        AXP_ERR("%s: id:%d selector:%d voltage:%d uV\n", __func__, id, selector, voltage );
    
    //AXP_DBG("%s: id:%d selector:%d voltage:%d uV\n", __func__, id, selector, voltage );
    return voltage;
}

static int axp192_common_set_voltage(struct regulator_dev *rdev, int min_uV, int max_uV, unsigned *selector)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 vol_reg  = axp_desc->vol_reg;
    unsigned const            vol_num = (unsigned)axp_desc->desc.n_voltages;
    int const                 vol_base = axp_desc->vol_base;
    int const                 vol_step = axp_desc->vol_step;
    int const                 vol_bpos = axp_desc->vol_bpos;
    int const                 vol_mask = axp_desc->vol_mask;
    int                       value = 0;
    int                       rc = 0;

    // value <--
    if ( vol_base < min_uV ) {
        value = ( min_uV - vol_base ) / vol_step;
        if ( vol_num <= value )
            value = vol_num - 1;
    }

    // write <-- value
    if ( vol_reg >= 0 )
        rc = AXP192_WRITE_BITS(regmap, vol_reg, vol_mask, value<<vol_bpos);

    // selector <--
    if ( rc == 0 && selector )
        *selector = value;

    //AXP_DBG("%s: id:%d rc:%d reg:0x%x voltage:%duV value=%d\n", __func__, id, rc, vol_reg, min_uV, value );
    return rc;
}

static int axp192_common_get_voltage_sel(struct regulator_dev *rdev)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 vol_reg  = axp_desc->vol_reg;
    unsigned const            vol_num = (unsigned)axp_desc->desc.n_voltages;
    //int const                 vol_base = axp_desc->vol_base;
    //int const                 vol_step = axp_desc->vol_step;
    int const                 vol_bpos = axp_desc->vol_bpos;
    int const                 vol_mask = axp_desc->vol_mask;
    int                       value = 0;
    int                       selector = -EINVAL;
    int                       rc;

    rc = AXP192_READ(regmap, vol_reg, &value);
    if (rc < 0)
        goto ret;

    value = ( value & vol_mask ) >> vol_bpos;
    if ( value < vol_num )
        selector = value;

  ret:
    //AXP_DBG("%s: rc:%d id:%d reg:0x%x selector:%d\n", __func__, rc, id, vol_reg, selector);
    return selector;
}

static int axp192_common_get_voltage(struct regulator_dev *rdev)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    //struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    //int const                 vol_reg  = axp_desc->vol_reg;
    unsigned const            vol_num = (unsigned)axp_desc->desc.n_voltages;
    int const                 vol_base = axp_desc->vol_base;
    int const                 vol_step = axp_desc->vol_step;
    //int const                 vol_bpos = axp_desc->vol_bpos;
    //int const                 vol_mask = axp_desc->vol_mask;

    int                       voltage = -EINVAL;
    int                       selector = axp192_common_get_voltage_sel( rdev );
    if ( selector < 0 || vol_num <= selector )
        goto ret;

    voltage = vol_base + vol_step * selector;

  ret:
    AXP_DBG("%s: id:%d voltage:%duV\n", __func__, id, voltage);
    return voltage;
}

static int axp192_common_enable_sub(struct regulator_dev *rdev, bool enable)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 en_reg  = axp_desc->en_reg;
    int const                 en_mask = axp_desc->en_mask;
    int                       rc;

    rc = AXP192_WRITE_BITS(regmap, en_reg, en_mask, enable ? en_mask : 0);

    AXP_DBG("%s: id:%d rc:%d en:%d\n", __func__, id, rc, enable);
    return rc;
}

static int axp192_common_enable(struct regulator_dev *rdev)
{
    return axp192_common_enable_sub(rdev, true);
}

static int axp192_common_disable(struct regulator_dev *rdev)
{
    return axp192_common_enable_sub(rdev, false);
}

static int axp192_common_is_enabled(struct regulator_dev *rdev)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 en_reg  = axp_desc->en_reg;
    int const                 en_mask = axp_desc->en_mask;
    int                       value;
    int                       rc;

    rc = AXP192_READ(regmap, en_reg, &value);
    if ( rc < 0 )
        goto ret;

    rc = ( value & en_mask ) != 0;

  ret:
    AXP_DBG("%s: id:%d rc:%d\n", __func__, id, rc);
    return rc;
}

//---------------------------------------
//  dcdc regulator
//---------------------------------------
#ifdef AXP192_DCDC2_VRC_SLOPE
/*
  It is required to define both 'get_voltage_sel' and 'set_voltage_time_sel'
  to insert wait after setting voltage. ( see _regulator_do_set_voltage() in regulator/core.c )
  But only one of 'get_voltage_sel' and 'get_voltage' should be defined.
  ( see regulator_register() in regulator/core.c )
  Otherwise, the following warning message is issued:
      WARNING: at drivers/regulator/core.c:3529 regulator_register+0xb70/0xd68()
      Modules linked in:
      CPU: 0 PID: 1 Comm: swapper/0 Not tainted 3.10.104 #653
      [<c0014150>] (unwind_backtrace+0x0/0xe0) from [<c00118ac>] (show_stack+0x10/0x14)
      [<c00118ac>] (show_stack+0x10/0x14) from [<c002dcbc>] (warn_slowpath_common+0x4c/0x6c)
      [<c002dcbc>] (warn_slowpath_common+0x4c/0x6c) from [<c002dd6c>] (warn_slowpath_null+0x18/0x20)
      [<c002dd6c>] (warn_slowpath_null+0x18/0x20) from [<c029ded4>] (regulator_register+0xb70/0xd68)
      [<c029ded4>] (regulator_register+0xb70/0xd68) from [<c02a0670>] (axp192_pmic_probe+0x4ac/0x848)
      [<c02a0670>] (axp192_pmic_probe+0x4ac/0x848) from [<c0429668>] (i2c_device_probe+0x98/0xd8)
      ...
  So, to insert wait,
    define:     get_voltage_sel, and set_voltage_time_sel,
    not-define: get_voltage.
 */
static int axp192_set_voltage_time_sel(struct regulator_dev* rdev, unsigned int old_selector, unsigned int new_selector)
{
    int       id = rdev_get_id(rdev);
    int       time = -EINVAL;

    if (id == AXP192_ID_DCDC2 && old_selector < AXP192_DCDC2_NUM && new_selector < AXP192_DCDC2_NUM ) {
        int     diff = abs( old_selector - new_selector );
        time = AXP192_DCDC2_VRC_time_per_step(AXP192_DCDC2_VRC_SLOPE) * diff / 1000;
    }

    //ret:
    //AXP_DBG("%s: id:%d old:%d new:%d time:%duS\n", __func__, id, old_selector, new_selector, time);
    return time;
}
#endif

//@@@@ for test
static int axp192_dcdc_disable(struct regulator_dev* AXP_UNUSED rdev)
{
    AXP_DBG("axp192_dcdc_disable\n");
    return 0;
}


//--------------------------------------
//  ldo regulator
//--------------------------------------
static int axp192_ldo4_enable_sub(struct regulator_dev *rdev, bool enable)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    //struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 en_reg  = AXP192_GPIO0_FUNC_REG;
    int const                 en_mask = AXP192_GPIO0_FUNC_MASK;
    int const                 en_value = enable ? AXP192_GPIO0_FUNC_LDO : AXP192_GPIO0_FUNC_FLOATING;
    int                       rc;

    rc = AXP192_WRITE_BITS(regmap, en_reg, en_mask, en_value);

    AXP_DBG("%s: id:%d rc:%d en:%d\n", __func__, id, rc, enable);
    return rc;
}

static int axp192_ldo4_enable(struct regulator_dev *rdev)
{
    return axp192_ldo4_enable_sub(rdev, true);
}

static int axp192_ldo4_disable(struct regulator_dev *rdev)
{
    return axp192_ldo4_enable_sub(rdev, false);
}

static int axp192_ldo4_is_enabled(struct regulator_dev *rdev)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    //struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    struct regmap*            regmap = axp192_regmap(axp_reg);
    AXP_UNUSED int const      id = rdev_get_id(rdev);
    int const                 en_reg  = AXP192_GPIO0_FUNC_REG;
    int const                 en_mask = AXP192_GPIO0_FUNC_MASK;
    int                       value;
    int                       rc = -EINVAL;

    rc = AXP192_READ(regmap, en_reg, &value);
    if ( rc < 0 )
        goto ret;

    rc = ( value & en_mask ) == AXP192_GPIO0_FUNC_LDO;

  ret:
    AXP_DBG("%s: id:%d rc:%d\n", __func__, id, rc);
    return rc;
}



//--------------------------------------
//  dummy  regulator
//--------------------------------------
#if AXP192_DUMMY_REGULATOR
static int axp192_dummy_set_voltage(struct regulator_dev *rdev, int min_uV, int max_uV, unsigned*pselector)
{
    struct axp192_regulator*  axp_reg = rdev_get_drvdata(rdev);
    //struct axp192_desc const* axp_desc = axp_reg->axp_desc;
    int     selector = 0;
    int     voltage;
    int     rc = axp192_common_set_voltage(rdev, min_uV, max_uV, &selector);
    if ( rc )
        goto ret;

    voltage = axp192_common_list_voltage( rdev, selector );
    if ( voltage < 0 ) {
        rc = voltage;
        goto ret;
    }
    axp_reg->curr_uV = voltage;
    if ( pselector )
        *pselector = selector;

  ret:
    AXP_DBG("%s: min:%d max:%d vol:%d uV\n", __func__, min_uV, max_uV, axp_reg->curr_uV );
    return rc;
}


static int axp192_dummy_get_voltage(struct regulator_dev *rdev)
{
    struct axp192_regulator*   axp_reg = rdev_get_drvdata(rdev);
    AXP_UNUSED int const      id = rdev_get_id(rdev);

    AXP_DBG("%s: voltage:%d \n", __func__, axp_reg->curr_uV );
    return axp_reg->curr_uV;
}

#endif

//--------------------------------------
//  initialize & finalize
//--------------------------------------
static struct regulator_ops axp192_dcdc13_ops = {
  .disable = axp192_dcdc_disable,
  .set_voltage = axp192_common_set_voltage,
  .list_voltage = axp192_common_list_voltage,

	// Only one of [get_voltage,get_voltage_sel] should be implemented.
  // Otherwise it triggers WARN_ON message at regulator_register() in regurator/core.c
  .get_voltage_sel = axp192_common_get_voltage_sel,
  //.get_voltage = axp192_common_get_voltage, 
};

static struct regulator_ops axp192_dcdc2_ops = {
  .disable = axp192_dcdc_disable,
  .set_voltage = axp192_common_set_voltage,
  .list_voltage = axp192_common_list_voltage,
#if AXP192_DCDC_WAIT_AFTER_SET
  .get_voltage_sel = axp192_common_get_voltage_sel,
  .set_voltage_time_sel = axp192_set_voltage_time_sel,
#else
  .get_voltage = axp192_common_get_voltage,
#endif
};

static struct regulator_ops axp192_ldo23_ops = {
  .set_voltage = axp192_common_set_voltage,
  .get_voltage = axp192_common_get_voltage,
  .list_voltage = axp192_common_list_voltage,
  .enable   = axp192_common_enable,
  .disable   = axp192_common_disable,
  .is_enabled  = axp192_common_is_enabled,
};

static struct regulator_ops axp192_ldo4_ops = {
  .set_voltage = axp192_common_set_voltage,
  .get_voltage = axp192_common_get_voltage,
  .list_voltage = axp192_common_list_voltage,
  .enable   = axp192_ldo4_enable,
  .disable   = axp192_ldo4_disable,
  .is_enabled  = axp192_ldo4_is_enabled,
};

static struct regulator_ops axp192_dummy_ops = {
  .set_voltage = axp192_dummy_set_voltage,
  .get_voltage = axp192_dummy_get_voltage,
  .list_voltage = axp192_common_list_voltage,
};

#define AXP192_REG_DESC(nm,ID,op,nvol) \
.desc = { \
    .name = #nm, \
    .id = AXP192_ID_##ID, \
    .ops = &op, \
    .type = REGULATOR_VOLTAGE, \
    .n_voltages = nvol, \
    .owner = THIS_MODULE, \
}
#define AXP192_DESC(reg,bpos,mask,base,step,e_reg,e_mask) \
    .vol_reg = reg, \
    .vol_bpos = bpos, \
    .vol_mask = mask, \
    .vol_base = base, \
    .vol_step = step, \
    .en_reg = e_reg, \
    .en_mask = e_mask,
    

static struct axp192_desc const axp192_descs[AXP192_ID_LAST+AXP192_DUMMY_REGULATOR] = {
    // dcdc1..3
    [AXP192_ID_DCDC1] = {
        AXP192_REG_DESC(dcdc1,DCDC1,axp192_dcdc13_ops,AXP192_DCDC1_NUM),
        AXP192_DESC(AXP192_DCDC1_REG,0,0x7f,AXP192_DCDC_BASE,AXP192_DCDC_STEP,AXP192_DCDC_LDO23_CTRL_REG,AXP192_DCDC_LDO23_CTRL_DCDC1)
    },
    [AXP192_ID_DCDC2] = {
        AXP192_REG_DESC(dcdc2,DCDC2,axp192_dcdc2_ops,AXP192_DCDC2_NUM),
        AXP192_DESC(AXP192_DCDC2_REG,0,0x3f,AXP192_DCDC_BASE,AXP192_DCDC_STEP,AXP192_DCDC_LDO23_CTRL_REG,AXP192_DCDC_LDO23_CTRL_DCDC2)
    },
    [AXP192_ID_DCDC3] = {
        AXP192_REG_DESC(dcdc3,DCDC3,axp192_dcdc13_ops,AXP192_DCDC3_NUM),
        AXP192_DESC(AXP192_DCDC3_REG,0,0x7f,AXP192_DCDC_BASE,AXP192_DCDC_STEP,AXP192_DCDC_LDO23_CTRL_REG,AXP192_DCDC_LDO23_CTRL_DCDC3)
    },

    // ldo2..4
    [AXP192_ID_LDO2] = {
        AXP192_REG_DESC(ldo2,LDO2,axp192_ldo23_ops,AXP192_LDO_NUM),
        AXP192_DESC(AXP192_LDO23_REG,4,0xf0,AXP192_LDO_BASE,AXP192_LDO_STEP,AXP192_DCDC_LDO23_CTRL_REG,AXP192_DCDC_LDO23_CTRL_LDO2)
    },
    [AXP192_ID_LDO3] = {
        AXP192_REG_DESC(ldo3,LDO3,axp192_ldo23_ops,AXP192_LDO_NUM),
        AXP192_DESC(AXP192_LDO23_REG,0,0x0f,AXP192_LDO_BASE,AXP192_LDO_STEP,AXP192_DCDC_LDO23_CTRL_REG,AXP192_DCDC_LDO23_CTRL_LDO3)
    },
    [AXP192_ID_LDO4] = {
        AXP192_REG_DESC(ldo4,LDO4,axp192_ldo4_ops,AXP192_LDO_NUM),
        AXP192_DESC(AXP192_LDO4_VOLTAGE_REG,4,0xf0,AXP192_LDO_BASE,AXP192_LDO_STEP,-1,-1)
    },

    // dummy
#if AXP192_DUMMY_REGULATOR
    [AXP192_ID_DUMMY] = {
        AXP192_REG_DESC(dummy,DUMMY,axp192_dummy_ops,AXP192_DUMMY_NUM),
        AXP192_DESC(-1,0,0,AXP192_DUMMY_BASE,AXP192_DUMMY_STEP,-1,-1)
    },
#endif
};

struct regulator_dev *axp192_regulator_register(
    struct regulator_desc const* regulator_desc,
    struct device* dev,
    struct regulator_init_data* init_data,
    void* driver_data)
{
      struct regulator_config config = {
      .dev = dev,
      .init_data = init_data,
      .driver_data = driver_data,
      .of_node = dev->of_node,
      };
    return regulator_register(regulator_desc, &config);
}

static struct regulator_init_data *axp192_parse_dt( struct axp192_regulator* axp_reg, struct device *dev)
{
    int         rc;
    int         volage;
    //int const   id = dev->id;  
    struct device_node*     of_node = dev->of_node;
    struct regulator_init_data* init_data = of_get_regulator_init_data(dev, of_node);
    if ( ! init_data ) {
        AXP_ERR("of_get_regulator_init_data fail\n");
        goto ret;
    }

    // initial_uV <--
    volage = init_data->constraints.min_uV;
    rc = of_property_read_u32( of_node, "axp,init_vol", &volage );
    axp_reg->initial_uV = volage;
    AXP_DBG("init_vol=%d(%s)\n", volage, !rc ? "specified" : "fallback to minVol" );

    // standby_uV <--
    volage = init_data->constraints.min_uV;
    rc = of_property_read_u32( of_node, "axp,standby_vol", &volage );
    axp_reg->standby_uV = volage;
    AXP_DBG("standby_vol=%d(%s)\n", volage, !rc ? "specified" : "fallback to minVol" );

  ret:
    AXP_DBG("%s %s\n", __func__, init_data ? "OK" : "fail" );
    return init_data;
}

static int axp192_regulator_probe(struct platform_device *pdev)
{
    int const   id = pdev->id;
    struct axp192_desc const*     axp_desc = &axp192_descs[id];
    struct regulator_desc const*  desc = &axp_desc->desc;
    struct axp192_core*           core = dev_get_drvdata(pdev->dev.parent);
    struct axp192_regulator*      axp_reg;
    struct regulator_init_data*   init_data;
    struct regulator_dev*         rdev = 0;
    int                           (*set_voltage) (struct regulator_dev*, int, int, unsigned*);
    int                           rc;

    //AXP_UNUSED void*    pdata = (pdev->dev.parent)->platform_data;
    //AXP_UNUSED void*    data = dev_get_drvdata(&pdev->dev);
    //AXP_DBG("name=%s core=%p pdata=%p mfd_cell=%p id=%d %d %d data=%p\n",
    //        pdev->name, core, pdev->mfd_cell, pdata,
    //        pdev->id, pdev->mfd_cell ? pdev->mfd_cell->id : -1, pdev->dev.id,
    //        data);
  
    // check id
    if ( id < 0 || ARRAY_SIZE(axp192_descs) <= id || id != axp_desc->desc.id ) {
        AXP_ERR( "invalid id:%d\n", id );
        rc = -ENODEV;
        goto ret;
    }
         
    // apx_reg <--
    if ( ( axp_reg = devm_kzalloc( &pdev->dev, sizeof(*axp_reg), GFP_KERNEL) ) == 0 ) {
        AXP_ERR( "devm_kzalloc fail\n" );
        rc = -ENOMEM;
        goto ret;
    }
    axp_reg->core = core;
    axp_reg->axp_desc = axp_desc;

    // init_data, axp_reg <-- dt resource
    init_data = axp192_parse_dt(axp_reg, &pdev->dev);
    if ( !init_data ) {
        AXP_ERR("no initializing data\n");
        rc = -EINVAL;
        goto ret;
    }

    // register 
    rdev = axp192_regulator_register(desc, &pdev->dev, init_data, axp_reg);
    if (IS_ERR(rdev)) {
    	AXP_ERR( "failed to register regulator %s\n", desc->name);
    	rc = PTR_ERR(rdev);
    	goto ret;
    }
    platform_set_drvdata(pdev, rdev);

    // set initial voltage
    set_voltage = desc->ops->set_voltage;
    rc = set_voltage( rdev, axp_reg->initial_uV, axp_reg->initial_uV, 0 );
    if ( rc ) {
        AXP_ERR("setting initial voltage fail(%s) rc:%d\n", desc->name, rc);
        goto ret;
    }
    AXP_DBG("set initial voltage(%s) %d uV\n", desc->name, axp_reg->initial_uV);


    // dcdc2 spcific case
#ifdef AXP192_DCDC2_VRC_SLOPE
    if ( id == AXP192_ID_DCDC2 ) {
        rc = AXP192_WRITE(core->regmap, AXP192_DCDC2_VRC_REG, AXP192_DCDC2_VRC_ENABLE | AXP192_DCDC2_VRC_SLOPE );
        if ( rc )
            AXP_ERR("setting vrc slope failed: id:%d rc:%d slope:%d\n", id, rc, AXP192_DCDC2_VRC_SLOPE );
        else {
            AXP_DBG("setting vrc slope: id:%d rc:%d slope:%d\n", id, rc, AXP192_DCDC2_VRC_SLOPE );
        }
    }
#endif    

  ret:
    if (rc) {
        if ( ! IS_ERR_OR_NULL(rdev) )
            regulator_unregister(rdev);
    }
    AXP_DBG("%s: id:%d rc:%d\n", __func__, id, rc);
    return rc;
}

static int axp192_regulator_remove(struct platform_device *pdev)
{
	struct regulator_dev *rdev = platform_get_drvdata(pdev);

	platform_set_drvdata(pdev, NULL);
	regulator_unregister(rdev);
	AXP_DBG("%s\n", __func__);
	return 0;
}

static struct of_device_id axp192_match_table[] = {
  { .compatible = AXP192_OF_COMATIBLE(dcdc1), },
  { .compatible = AXP192_OF_COMATIBLE(dcdc2), },
  { .compatible = AXP192_OF_COMATIBLE(dcdc3), },
  //{ .compatible = AXP192_OF_COMATIBLE(ldo1),},
  { .compatible = AXP192_OF_COMATIBLE(ldo2),  },
  { .compatible = AXP192_OF_COMATIBLE(ldo3),  },
  { .compatible = AXP192_OF_COMATIBLE(ldo4),  },
  { .compatible = AXP192_OF_COMATIBLE(dummy), },
	{},
};


static struct platform_driver axp192_regulator_driver = {
	.driver = {
		.name = AXP192_DEV_NAME "-regulator",
		.owner = THIS_MODULE,
		.of_match_table = axp192_match_table,
	},
	.probe = axp192_regulator_probe,
	.remove = axp192_regulator_remove,
};

static int axp192_regulator_init(void)
{
  AXP_DBG("axp192_regulator_init\n");
  AXP_DBG("axp192_match_table=%p\n", &axp192_match_table[0]);


	return platform_driver_register(&axp192_regulator_driver);
}
subsys_initcall(axp192_regulator_init);
//subsys_initcall_sync(axp192_regulator_init);

static void axp192_regulator_exit(void)
{
	platform_driver_unregister(&axp192_regulator_driver);
}
module_exit(axp192_regulator_exit);
