/*
 *  drivers/gpio/gpio-axp192.c
 *  ( based on drivers/gpio/gpio-rt5025.c )
 *  Driver for X-Power AXP192 PMIC GPIO
 *
 *  Copyright (C) 2014 Richtek Technologh Corp.
 *  cy_huang <cy_huang@richtek.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * modifed for AXP192 in T-station
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <linux/of.h>

#include <linux/regmap.h>
#include <linux/mfd/axp192.h>

#ifndef CONFIG_OF
#error CONFIG_OF must be defined
#endif

//**************************************
// configulation
//**************************************
// enable verbose message
#define AXP192_DEBUG


// initial register setting
static const struct initial_setting {
  int     reg;
  int     value;
} initial_settings[] = {

#if 0 // default value
  { // gpio1 == floating
    AXP192_GPIO1_FUNC_SET_REG,
    AXP192_GPIO1_FUNC_FLOATING
  },
  { // gpio2 == floating
    AXP192_GPIO2_FUNC_SET_REG,
    AXP192_GPIO2_FUNC_FLOATING
  },
  { // gpio34 == floating
    AXP192_GPIO34_FUNC_SET_REG,
    0
  },
#endif  // 0
};

//**************************************
// struct
//**************************************
struct axp192_gpio {
    struct axp192_core*   core;
    struct gpio_chip      gpio_chip;
};

//**************************************
// debug
//**************************************
#ifdef AXP192_DEBUG

#define AXP_DBG(msg...) printk("AXP192(gpio): " msg)

// AXP_CALL macro requires that 'int line' is defined.
#define AXP_CALL(fnc,...) (line=__LINE__, fnc(__VA_ARGS__))

#else
#define AXP_DBG(msg...)
#define AXP_CALL(fnc,...) fnc(__VA_ARGS__)
#endif
#define AXP_ERR(msg...) printk("AXP192(gpio): Error " msg)

#define AXP_UNUSED    __attribute__((unused))


//**************************************
// bus access
//**************************************
static inline struct regmap* axp192_regmap( struct axp192_gpio* apx_gpio )
{
    return apx_gpio->core->regmap;
}

//**************************************
// constant
//**************************************
static const struct axp192_gpio_setting {
  int       ctrl_reg;
  int       ctrl_mask;
  int       ctrl_input;
  int       ctrl_output;
  int       ctrl_float;
  
  int       status_reg;
  int       status_mask_in;
  int       status_mask_out;
} axp_gpio_settings[] = {
#if 0 
  // gpio 0 is disabled ( It's assinged as LDO )
  { AXP192_GPIO0_FUNC_REG,
    AXP192_GPIO0_FUNC_MASK,
    AXP192_GPIO0_FUNC_INPUT,
    AXP192_GPIO0_FUNC_OPEN_DRAIN,
    AXP192_GPIO0_FUNC_FLOATING,

    AXP192_GPIO012_STATUS_REG,
    AXP192_GPIO012_STATUS_0_IN,
    AXP192_GPIO012_STATUS_0_OUT
  },
#endif  // 0

  // gpio 1
  { AXP192_GPIO1_FUNC_SET_REG,
    AXP192_GPIO1_FUNC_MASK,
    AXP192_GPIO1_FUNC_INPUT,
    AXP192_GPIO1_FUNC_OPEN_DRAIN,
    AXP192_GPIO1_FUNC_FLOATING,

    AXP192_GPIO012_STATUS_REG,
    AXP192_GPIO012_STATUS_1_IN,
    AXP192_GPIO012_STATUS_1_OUT,
  },

  // gpio 2
  { AXP192_GPIO2_FUNC_SET_REG,
    AXP192_GPIO2_FUNC_MASK,
    AXP192_GPIO2_FUNC_INPUT,
    AXP192_GPIO2_FUNC_OPEN_DRAIN,
    AXP192_GPIO2_FUNC_FLOATING,

    AXP192_GPIO012_STATUS_REG,
    AXP192_GPIO012_STATUS_2_IN,
    AXP192_GPIO012_STATUS_2_OUT,
  },

  // gpio3
  { AXP192_GPIO34_FUNC_SET_REG,
    AXP192_GPIO34_FUNC_3_MASK | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_3_INPUT | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_3_OPEN_DRAIN | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_3_EXTCHARGE, //??

    AXP192_GPIO34_STATUS_REG,
    AXP192_GPIO34_STATUS_3_IN,
    AXP192_GPIO34_STATUS_3_OUT,
  },
  
  // gpio4
  { AXP192_GPIO34_FUNC_SET_REG,
    AXP192_GPIO34_FUNC_4_MASK | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_4_INPUT | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_4_OPEN_DRAIN | AXP192_GPIO34_FUNC_GPIO,
    AXP192_GPIO34_FUNC_4_EXTCHARGE, //??

    AXP192_GPIO34_STATUS_REG,
    AXP192_GPIO34_STATUS_4_IN,
    AXP192_GPIO34_STATUS_4_OUT,
  },
};

//**************************************
// interface functions
//**************************************
/*
  return 0 : if OK
 */
static int axp192_check_offset(unsigned offset, char const* strFnc)
{
    int const     rc = ARRAY_SIZE(axp_gpio_settings) <= offset ? -EINVAL : 0;
    if ( rc )
        AXP_ERR("%s invalid offset:%d\n", strFnc, offset);
    return rc;
}

static int axp192_gpio_direction_input(struct gpio_chip* chip, unsigned offset)
{
    struct axp192_gpio*   axp_gpio = dev_get_drvdata(chip->dev);
    struct regmap*        regmap = axp192_regmap(axp_gpio);
    const struct axp192_gpio_setting*  setting = &axp_gpio_settings[offset];
    int                   rc;
    int                   line;

    // check offset, and set direction
    if ( ( rc = AXP_CALL(axp192_check_offset, offset, __func__) ) < 0 ||
         ( rc = AXP_CALL(regmap_update_bits, regmap, setting->ctrl_reg, setting->ctrl_mask, setting->ctrl_input) ) < 0 ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }
      
  ret:
    AXP_DBG("%s rc:%d\n", __func__, rc);
    return rc;
}

static int axp192_gpio_direction_output(struct gpio_chip* chip, unsigned offset, int value)
{
    struct axp192_gpio*   axp_gpio = dev_get_drvdata(chip->dev);
    struct regmap*        regmap = axp192_regmap(axp_gpio);
    const struct axp192_gpio_setting*  setting = &axp_gpio_settings[offset];
    int                   rc;
    int                   line;

    // check offset, prepare initial value and set direction
    if ( ( rc = AXP_CALL(axp192_check_offset, offset, __func__) ) < 0 ||
         ( rc = AXP_CALL(regmap_update_bits, regmap, setting->status_reg, setting->status_mask_out,
                         value ? setting->status_mask_out : 0) ) < 0 ||
         ( rc = AXP_CALL(regmap_update_bits, regmap, setting->ctrl_reg, setting->ctrl_mask, setting->ctrl_output) ) < 0 
        ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }

  ret:
    AXP_DBG("%s rc:%d value:%d\n", __func__, rc, value);
    return rc;
}

static int axp192_gpio_get_value(struct gpio_chip* chip, unsigned offset)
{
    struct axp192_gpio*   axp_gpio = dev_get_drvdata(chip->dev);
    struct regmap*        regmap = axp192_regmap(axp_gpio);
    const struct axp192_gpio_setting*  setting = &axp_gpio_settings[offset];
    int                   rc;
    int                   line;
    int                   value;

    // check offset, and read value
    if ( ( rc = AXP_CALL(axp192_check_offset, offset, __func__) ) < 0 ||
         ( rc = AXP_CALL(regmap_read, regmap, setting->status_reg, &value) ) < 0 ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }
    rc = value & setting->status_mask_in ? 1 : 0;

  ret:
    AXP_DBG("%s rc:%d\n", __func__, rc);
    return rc;
}

static void axp192_gpio_set_value(struct gpio_chip* chip, unsigned offset, int value)
{
    struct axp192_gpio*   axp_gpio = dev_get_drvdata(chip->dev);
    struct regmap*        regmap = axp192_regmap(axp_gpio);
    const struct axp192_gpio_setting*  setting = &axp_gpio_settings[offset];
    int                   rc;
    int                   line;

    // check offset, and write value
    if ( ( rc = AXP_CALL(axp192_check_offset, offset, __func__) ) < 0 ||
         ( rc = AXP_CALL(regmap_update_bits, regmap, setting->status_reg, setting->status_mask_out,
                         value ? setting->status_mask_out : 0 ) ) < 0 ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }

  ret:
    AXP_DBG("%s rc:%d value:%d\n", __func__, rc, value);
    //return rc;
}

static int axp192_gpio_probe(struct platform_device *pdev)
{
    struct axp192_core*     core = dev_get_drvdata(pdev->dev.parent);
    struct regmap*          regmap = core->regmap;
    struct axp192_gpio*     axp_gpio;
    int                     rc = -ENOMEM;
    int                     line;
    int                     i;

    // axp_gpio <--
    if ( ( axp_gpio = AXP_CALL(devm_kzalloc, &pdev->dev, sizeof(*axp_gpio), GFP_KERNEL) ) == 0 ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }
    axp_gpio->core = core;
    axp_gpio->gpio_chip.direction_input  = axp192_gpio_direction_input;
    axp_gpio->gpio_chip.direction_output = axp192_gpio_direction_output;
    axp_gpio->gpio_chip.get = axp192_gpio_get_value;
    axp_gpio->gpio_chip.set = axp192_gpio_set_value;
    axp_gpio->gpio_chip.can_sleep = 0;
    axp_gpio->gpio_chip.base = -1;
    axp_gpio->gpio_chip.ngpio = ARRAY_SIZE(axp_gpio_settings);
    axp_gpio->gpio_chip.label = pdev->name;
    axp_gpio->gpio_chip.dev = &pdev->dev;
    axp_gpio->gpio_chip.owner = THIS_MODULE;

    // initial register setting
    for ( i = 0 ; i < ARRAY_SIZE(initial_settings) ; i ++ ) {
        const struct initial_setting* setting = &initial_settings[i];
        if ( ( rc = AXP192_WRITE(regmap, setting->reg, setting->value) ) < 0 ) {
            AXP_ERR("initial_setting fail rc:%d\n", rc);
            goto ret;
        }
    }

    // add
    if ( ( rc = AXP_CALL(gpiochip_add, &axp_gpio->gpio_chip) ) < 0 ) {
        AXP_ERR("rc:%d line:%d\n", rc, line);
        goto ret;
    }

    platform_set_drvdata(pdev, axp_gpio);

  ret:
    AXP_DBG("%s rc:%d\n", __func__, rc);
    return rc;
}

static int axp192_gpio_remove(struct platform_device *pdev)
{
    struct axp192_gpio* axp_gpio = platform_get_drvdata(pdev);
    int                 rc;

    rc = gpiochip_remove(&axp_gpio->gpio_chip);
    AXP_DBG("%s rc:%d\n", __func__, rc);
    return 0;
}

static struct of_device_id rt_match_table[] = {
    { .compatible = AXP192_OF_COMATIBLE(gpio),},
    {},
};

static struct platform_driver axp192_gpio_driver = {
    .driver = {
		.name = AXP192_DEV_NAME "-gpio",
		.owner = THIS_MODULE,
		.of_match_table = rt_match_table,
	},
	.probe = axp192_gpio_probe,
	.remove = axp192_gpio_remove,
};

static int axp192_gpio_init(void)
{
	return platform_driver_register(&axp192_gpio_driver);
}
fs_initcall_sync(axp192_gpio_init);

static void axp192_gpio_exit(void)
{
	platform_driver_unregister(&axp192_gpio_driver);
}
module_exit(axp192_gpio_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CY Huang <cy_huang@richtek.com>");
MODULE_DESCRIPTION("GPIO driver for AXP192");
MODULE_ALIAS("platform:" AXP192_DEV_NAME "-gpio");
MODULE_VERSION(AXP192_DRV_VER);
