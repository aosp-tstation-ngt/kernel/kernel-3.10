/*
 *  drivers/mfd/axp192-core.c
 *  Driver for X-Power AXP192 PMIC(mfd part)
 *
 *  ( based on drivers/mfd/rt5025-core.c
 *    modified for AXP192 on T-station)
 *
 *  Copyright (C) 2019 T-Station test
 *  T-Station test <tstation@tstation-test.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/i2c.h>
#include <linux/mfd/core.h>
#include <linux/regmap.h>
#include <linux/delay.h>

#define AXP192_DEBUG
#include <linux/mfd/axp192.h>


#ifndef CONFIG_OF
#error CONFIG_OF must be defined
#endif

//**************************************
// configulation
//**************************************
// enable verbose message
#define AXP192_DEBUG

// i2c speed
#define AXP192_I2C_SPEED      (100 * 1000)

// include irq part
#define CONFIG_IRQ_AXP192

// initial register setting
const struct initial_setting {
  int     reg;
  int     value;
} initial_settings[] = {
  { AXP192_POWER_OFF_VOL_REG, 
    AXP192_PWROFF_VOL_2_6V          // auto power off when IPS < 2.6V
  },
  { AXP192_POWER_OFF_REG, 0
    | AXP192_PWROFF_CHGLED_HIGHZ    // CHGLED pin == hi-z
    | AXP192_PWROFF_CHGLED_CTRL_REG // CHGLED pin is controled by AXP192_POWER_OFF_REG(AXP192_PWROFF_CHGLED_xx)
    | AXP192_PWROFF_N_OE_DLY_3_0S   // shutdown delay Late time(when N_OE lo-hi)
  },
  { AXP192_PEK_REG, 0
    | AXP192_PEK_PWRON_1S
    | AXP192_PEK_LONGKEY_2_5S       // trigger long press irq when PEK pressing time > 2.5sec
    | AXP192_PEK_LONGKEY_PWROFF_EN  // enable shutdown when PEK pressing time exceeds AXP192_PEK_PWROFF_TIME_xx
    | AXP192_PEK_PWRON_DLY_64mS     // activate PWROK with 64mSec delay after power up
    | AXP192_PEK_PWROFF_TIME_6S     // shutdown when PEK pressing time > 6sec
  },
  { AXP192_ADC_ENABLE1_REG, 0
    | AXP192_ADC_1_VBUS_VOL         // enable VBUS Voltage ADC function
  },
  { AXP192_ADC_ENABLE2_REG, 0       // disable ADC function of GPIO0..3
  },
  { AXP192_ADC_TSPIN_REG, 0
    | AXP192_ADC_TS_SAMPLE_25       // ADC Sample Rate == 25Hz
    | AXP192_ADC_TS_OUT_CUR_20uA    // TS pin output current == 20uA 
    | AXP192_ADC_TS_PIN_EXTERN_ADC  // TS pin == External independent ADC input path
    | AXP192_ADC_TS_PIN_OUT_DIS     // TS pin current output mode == off
  },
  { AXP192_DCDC123_MODE_REG, 0
    | AXP192_DCDC1_MODE_FIXED       // dcdc1 operating mode == fixed PWM
    | AXP192_DCDC2_MODE_FIXED       // dcdc2 operating mode == fixed PWM
    | AXP192_DCDC3_MODE_FIXED       // dcdc3 operating mode == fixed PWM
  },
  { AXP192_VBUS_IPSOUT_REG, 0
    //| AXP192_VBUS_IPSOUT_BY_N_VBUSEN  // disable VBUSEN pin to select VBUS-IPSOUT path
    | AXP192_VBUS_IPSOUT_ALWAYS_ON      // enable always VBUS-IPSOUT path
    //| AXP192_VBUS_VHOLD_LIMIT_EN      // disable VBUS voltage constraints(VBUS voltage>VHOLD) 
    | AXP192_VBUS_VHOLD_VOL_4_4         // VHOLD(VBUS voltage constraint, VBUS > VHOLD) == 4.4V
    //| AXP192_VBUS_CURRENT_LIMIT_EN    // disable VBUS current constraints(<AXP192_VBUS_CURRENT_LIMIT_xx)
  },
  { AXP192_VBUS_IPSOUT_REG,
    AXP192_OVER_TEMP_SHUTDOWN       // enable shutdown when over temperature occurs
  },

  // gpio
  //{ AXP192_GPIO34_FUNC_SET_REG, 0
  //  | AXP192_GPIO34_FUNC_GPIO
  //  | AXP192_GPIO34_FUNC_INPUT4
  //  | AXP192_GPIO34_FUNC_NMOS_OUT3
  //},      
  //{ AXP192_GPIO34_IO_REG, 0
  //  | AXP192_GPIO34_IO_OUTPUT3
  //},
};


//**************************************
// debug
//**************************************
#ifdef AXP192_DEBUG
#define AXP_DBG(msg...) printk("AXP192(core): " msg)
#else
#define AXP_DBG(msg...)
#endif
#define AXP_ERR(msg...) printk("AXP192(core): Error " msg)

#define AXP_UNUSED    __attribute__((unused))

//**************************************
// bus access
//**************************************
static inline struct regmap* axp192_regmap( struct axp192_core* core )
{
    return core->regmap;
}


//**************************************
// mfd
//**************************************
#define AXP192_REGULATOR_DEV(SUFFIX,suffix)      \
{									\
	.name		= AXP192_DEV_NAME "-regulator",		\
	.num_resources	= 0,						\
	.of_compatible	= AXP192_OF_COMATIBLE(suffix), \
	.id		= AXP192_ID_##SUFFIX,				\
}

static struct mfd_cell mfd_cells[] = {
#ifdef CONFIG_REGULATOR_AXP192
    AXP192_REGULATOR_DEV(DCDC1,dcdc1),
    AXP192_REGULATOR_DEV(DCDC2,dcdc2),
    AXP192_REGULATOR_DEV(DCDC3,dcdc3),
    AXP192_REGULATOR_DEV(LDO2,ldo2),
    AXP192_REGULATOR_DEV(LDO3,ldo3),
    AXP192_REGULATOR_DEV(LDO4,ldo4),
    AXP192_REGULATOR_DEV(DUMMY,dummy),
#endif //CONFIG_REGULATOR_AXP192
#ifdef CONFIG_GPIO_AXP192
    {
        .name = AXP192_DEV_NAME "-gpio",
        .id = -1,
        .num_resources = 0,
        .of_compatible	= AXP192_OF_COMATIBLE(gpio),
    },
#endif //CONFIG_GPIO_AXP192
#ifdef CONFIG_IRQ_AXP192
    {
        .name = AXP192_DEV_NAME "-irq",
        .id = -1,
        .num_resources = 0,
        .of_compatible	= AXP192_OF_COMATIBLE(irq),
    },
#endif // CONFIG_IRQ_AXP192
};

//**************************************
// i2c
//**************************************
static struct i2c_client* apx192_i2c = 0;
static int apx192_i2c_byte_send( struct i2c_client *client, char reg, char val )
{
    struct i2c_adapter*   adap=client->adapter;
    struct i2c_msg        msg;
    char                  tx_buf[2] = { reg, val };
    int                   ret;

    msg.addr = client->addr;
    msg.flags = client->flags;
    msg.len = 2;
    msg.buf = &tx_buf[0];
    msg.scl_rate = AXP192_I2C_SPEED;

    ret = i2c_transfer(adap, &msg, 1);
    return ret < 0 ? ret : 0;
}

static void axp192_power_off(void)
{
    int     rc;
    int     value =
        AXP192_PWROFF_EN |
        AXP192_PWROFF_BATT_MON_DIS |
        AXP192_PWROFF_CHGLED_CTRL_REG |
        AXP192_PWROFF_CHGLED_HIGHZ;
    AXP_DBG("%s\n", __func__);

    //@@@@ for test
    while ( ( rc = apx192_i2c_byte_send(apx192_i2c, AXP192_POWER_OFF_REG, value) ) < 0 ) {
        AXP_DBG("write AXP192_POWER_OFF_REG fail rc:%d\n", rc );
        mdelay(100);
    }

    while(1) wfi();
}

static int axp192_i2c_probe(struct i2c_client* i2c, const struct i2c_device_id *id)
{
    struct axp192_core*     core;
    struct device_node*     of_node = i2c->dev.of_node;
    unsigned int            status;
    const struct regmap_config    regmap_cfg = {
        .reg_bits = 8,
        .val_bits = 8,
        .cache_type = REGCACHE_NONE,
    };
    int       rc;
    int       i;

    AXP_DBG("axp192_i2c_probe\n");

    // core <--
    if ( ( core = devm_kzalloc(&i2c->dev, sizeof(*core), GFP_KERNEL) ) == 0 ) {
        AXP_ERR( "devm_kzalloc fail\n" );
        rc = -ENOMEM;
        goto ret;
    }
    //...core->regmap <-- iniialize regmap
    if ( IS_ERR_OR_NULL( core->regmap = devm_regmap_init_i2c(i2c, &regmap_cfg) ) )  {
        rc = core->regmap ? PTR_ERR(core->regmap) : -ENOMEM;
        AXP_ERR( "devm_regmap_init_i2c fail %d\n", rc );
        goto ret;
    }
    core->i2c = i2c;
    core->dev = &i2c->dev;
    i2c_set_clientdata(i2c, core);

    // check status
    if ( ( rc = regmap_read( core->regmap, AXP192_POWER_STS_REG, &status ) ) != 0 ) {
        AXP_ERR( "read status fail %d\n", rc );
        goto ret;
    }
    AXP_DBG("status=%x\n", status);
  

    // initial setting
    for ( i = 0 ; i < ARRAY_SIZE(initial_settings) ; i ++ ) {
      const struct initial_setting*   setting = &initial_settings[i];
      rc = AXP192_WRITE(axp192_regmap(core), setting->reg, setting->value);
      if ( rc ) {
        AXP_ERR("initial setting reg:%x value:%x\n", setting->reg, setting->value);
        goto ret;
      }
    }

    //@@@@ for debug
    {
        int   value, rc2;
        rc2 = AXP192_READ(axp192_regmap(core), AXP192_OVER_TEMP_REG, &value);
        AXP_DBG("AXP192_OVER_TEMP_REG rc:%d val:%x\n", rc2, value);
    }

    // add mfd sub devs
    rc = mfd_add_devices(core->dev, 0, &mfd_cells[0], ARRAY_SIZE(mfd_cells), NULL, 0, NULL);
    if ( rc < 0 ) {
        AXP_ERR("mfd_add_devices fail %d\n", rc);
        goto ret;
    }

    // register power off shutdown 
    if ( !pm_power_off && of_property_read_bool(of_node, "axp192,system-power-controller") ) {
        AXP_DBG("register pm_power_off\n");
        apx192_i2c = i2c;
        pm_power_off = axp192_power_off;
    }

  ret:
    if ( rc ) {
        AXP_ERR( "axp192_i2c_probe %d)\n", rc );
    }

    //AXP_DBG("i2c=%p pdata=%p core=%p\n", i2c, pdata, core );
    AXP_DBG("axp192_i2c_probe %d\n", rc);
    return rc;
}

static int axp192_i2c_remove(struct i2c_client* i2c)
{
	struct axp192_core* core = i2c_get_clientdata(i2c);
  mfd_remove_devices(core->dev);

	AXP_DBG("axp192_i2c_remove\n");
	return 0;
}

static int axp192_i2c_suspend(struct device *dev)
{
	AXP_DBG("axp192_i2c_suspend\n");
	return 0;
}

static int axp192_i2c_resume(struct device *dev)
{
	AXP_DBG("axp192_i2c_resume\n");
	return 0;
}

static const struct dev_pm_ops axp192_pm_ops = {
	.suspend = axp192_i2c_suspend,
	.resume =  axp192_i2c_resume,
};

static const struct i2c_device_id axp192_id_table[] = {
	{ AXP192_DEV_NAME, 0 },
	{ },
};
MODULE_DEVICE_TABLE(i2c, axp192_id_table);

static struct of_device_id axp_match_table[] = {
    { .compatible = AXP192_OF_COMATIBLE(core) },
    {},
};

static struct i2c_driver axp192_i2c_driver = {
	.driver	= {
		.name	= AXP192_DEV_NAME,
		.owner	= THIS_MODULE,
		 .pm = &axp192_pm_ops,
		.of_match_table = axp_match_table,
	},
	.probe		= axp192_i2c_probe,
	.remove		= axp192_i2c_remove,
	.id_table	= axp192_id_table,
};

static int axp192_i2c_init(void)
{
  AXP_DBG("axp192_i2c_init\n");
	return i2c_add_driver(&axp192_i2c_driver);
}
subsys_initcall_sync(axp192_i2c_init);
//subsys_initcall(axp192_i2c_init);

static void axp192_i2c_exit(void)
{
	i2c_del_driver(&axp192_i2c_driver);
}
module_exit(axp192_i2c_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("I2C Driver for X-Power AXP192");
MODULE_AUTHOR("T-Station test <tstation@tstation-test.com>");
MODULE_VERSION(AXP192_DRV_VER);
