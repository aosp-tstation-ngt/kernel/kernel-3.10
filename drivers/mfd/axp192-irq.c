/*
 *  drivers/mfd/axp192-irq.c
 *
 *  Copyright (C) 2011 by Telechips
 *
 *  This program is free software;
 *
 *  modified for T-station
 *
 *  download site:
 *  https://github.com/cnxsoft/telechips-linux/blob/master/drivers/regulator/axp192.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/of_gpio.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>
#include <linux/regmap.h>
#include <linux/input.h>
#include <linux/irq.h>

#include <linux/mfd/axp192.h>

//**************************************
//  configulation
//**************************************
// verbose log
#define AXP192_DEBUG


// add input device of power key, and report pek short/long press.
//#define AXP192_REPORT_POWER_KEY


// enabled irq 1..4
#define AXP192_IRQ1 ( 0                               \
                      | AXP192_IRQ_1_ACIN_OVER        \
                      | AXP192_IRQ_1_VBUS_OVER        \
                      | AXP192_IRQ_1_VBUS_LT_VHOLD \
        )
#define AXP192_IRQ2 ( 0                         \
        )
#define AXP192_IRQ3 ( 0                             \
                      | AXP192_IRQ_3_INTERNAL_OVER  \
                      | AXP192_IRQ_3_DCDC1_UNDER    \
                      | AXP192_IRQ_3_DCDC2_UNDER    \
                      | AXP192_IRQ_3_DCDC3_UNDER    \
                      | AXP192_IRQ_3_SHORT_KEY      \
                      | AXP192_IRQ_3_LONG_KEY       \
        )
#define AXP192_IRQ4 ( 0                             \
                      | AXP192_IRQ_4_POWER_ON       \
                      | AXP192_IRQ_4_POWER_OFF      \
                      | AXP192_IRQ_4_UNDER_VOLTAGE  \
        )


//**************************************
// debug
//**************************************
#ifdef AXP192_DEBUG

#define AXP_DBG(msg...) printk("AXP192(irq): " msg)

// AXP_CALL macro requires that 'int line' is defined.
#define AXP_CALL(fnc,...) (line=__LINE__, fnc(__VA_ARGS__))

#else
#define AXP_DBG(msg...)
#define AXP_CALL(fnc,...) fnc(__VA_ARGS__)
#endif
#define AXP_ERR(msg...) printk("AXP192(irq): Error " msg)

#define AXP_UNUSED    __attribute__((unused))


//**************************************
//  struct
//**************************************
struct axp192_irq {
    struct axp192_core*     core;
    struct device*          dev;

    int                     irq;
    int                     suspend;

    struct work_struct      work;
    struct workqueue_struct*work_que;

    int                     acin_det;
    int                     charge_sts;
    unsigned int            suspend_status;

#if defined(AXP192_REPORT_POWER_KEY)
    struct input_dev*       input_dev;
    struct timer_list       timer;
#endif
};

//**************************************
// bus access
//**************************************
void  apx192_clear_status(struct regmap*regmap)
{
    AXP192_WRITE(regmap, AXP192_IRQ_STS1_REG, 0xFF);
    AXP192_WRITE(regmap, AXP192_IRQ_STS2_REG, 0xFF);
    AXP192_WRITE(regmap, AXP192_IRQ_STS3_REG, 0xFF);
    AXP192_WRITE(regmap, AXP192_IRQ_STS4_REG, 0xFF);
}

void  apx192_irq_enable(struct regmap*regmap)
{
    AXP192_WRITE(regmap, AXP192_IRQ_EN1_REG, AXP192_IRQ1);
    AXP192_WRITE(regmap, AXP192_IRQ_EN2_REG, AXP192_IRQ2);
    AXP192_WRITE(regmap, AXP192_IRQ_EN3_REG, AXP192_IRQ3);
    AXP192_WRITE(regmap, AXP192_IRQ_EN4_REG, AXP192_IRQ4);
}

void  apx192_irq_disable(struct regmap*regmap)
{
    AXP192_WRITE(regmap, AXP192_IRQ_EN1_REG, 0);
    AXP192_WRITE(regmap, AXP192_IRQ_EN2_REG, 0);
    AXP192_WRITE(regmap, AXP192_IRQ_EN3_REG, 0);
    AXP192_WRITE(regmap, AXP192_IRQ_EN4_REG, 0);
}

static inline struct regmap* axp192_regmap( struct axp192_irq* axp_irq )
{
    return axp_irq->core->regmap;
}

//-----------------------------------------
//  interrupt routine related
//-----------------------------------------

static void axp192_work_func(struct work_struct *work)
{
    struct axp192_irq*  axp_irq = container_of(work, struct axp192_irq, work);
    struct regmap*      regmap = axp192_regmap(axp_irq);
    int                 rc[4];
    unsigned int        data[4];

    AXP_DBG("%s\n", __func__);
    rc[0] = AXP192_READ(regmap, AXP192_IRQ_STS1_REG, &data[0]);
    rc[1] = AXP192_READ(regmap, AXP192_IRQ_STS2_REG, &data[1]);
    rc[2] = AXP192_READ(regmap, AXP192_IRQ_STS3_REG, &data[2]);
    rc[3] = AXP192_READ(regmap, AXP192_IRQ_STS4_REG, &data[3]);

    if ( rc[0] | rc[1] | rc[2] | rc[3] ) {
        AXP_ERR("%s rc:%d %d %d %d\n", __func__, rc[0], rc[1], rc[2], rc[3] );
        goto ret;
    }

    if (data[0]&AXP192_IRQ_1_VBUS_LT_VHOLD) {
        AXP_DBG("VBUS valid, but lower then V(hold)IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_VBUS_REMOVE) {
        AXP_DBG("VBUS remove IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_VBUS_INSERT) {
        AXP_DBG("VBUS insert IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_VBUS_OVER) {
        AXP_DBG("VBUS over voltage IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_ACIN_REMOVE) {
        axp_irq->acin_det = 0;
        axp_irq->charge_sts = AXP192_CHG_NONE;
        AXP_DBG("ACIN remove IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_ACIN_INSERT) {
        axp_irq->acin_det = 1;
        AXP_DBG("ACIN insert IRQ status\n");
    }
    if (data[0]&AXP192_IRQ_1_ACIN_OVER) {
        AXP_DBG("ACIN over voltage IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_BATT_UNDER) {
        AXP_DBG("Battery under temperature IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_BATT_OVER) {
        AXP_DBG("Battery over temperature IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_CHARGE_FINISH) {
        AXP_DBG("Charge finish IRQ status\n");
        axp_irq->charge_sts = AXP192_CHG_OK;
    }
    if (data[1]&AXP192_IRQ_2_CHARGING) {
        AXP_DBG("Charging IRQ status\n");
        axp_irq->charge_sts = AXP192_CHG_ING;
    }
    if (data[1]&AXP192_IRQ_2_BATT_QUIT_ACT) {
        AXP_DBG("Quit battery active mode IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_BATT_ACT) {
        AXP_DBG("Battery active mode IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_BATT_REMOVE) {
        AXP_DBG("Battery remove IRQ status\n");
    }
    if (data[1]&AXP192_IRQ_2_BATT_INSERT) {
        AXP_DBG("Battery insert IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_LONG_KEY) {
#if defined(AXP192_REPORT_POWER_KEY)
        del_timer(&axp_irq->timer);
        input_report_key(axp_irq->input_dev, KEY_POWER, 1);
        //input_event(axp_irq->input_dev, EV_KEY, KEY_POWER, 1);
        input_event(axp_irq->input_dev, EV_SYN, 0, 0);
        axp_irq->timer.expires = jiffies + msecs_to_jiffies(1000);
        add_timer(&axp_irq->timer);
#endif
        AXP_DBG("Long time key press IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_SHORT_KEY) {
#if defined(AXP192_REPORT_POWER_KEY)
        del_timer(&axp_irq->timer);
        input_report_key(axp_irq->input_dev, KEY_POWER, 1);
        //input_event(axp_irq->input_dev, EV_KEY, KEY_POWER, 1);
        input_event(axp_irq->input_dev, EV_SYN, 0, 0);
        axp_irq->timer.expires = jiffies + msecs_to_jiffies(100);
        add_timer(&axp_irq->timer);
#endif
        AXP_DBG("Short time key press IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_DCDC3_UNDER) {
        AXP_DBG("DC-DC3 under voltage IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_DCDC2_UNDER) {
        AXP_DBG("DC-DC2 under voltage IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_DCDC1_UNDER) {
        AXP_DBG("DC-DC1 under voltage IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_NOT_ENOUGH_CURR) {
        AXP_DBG("Charge current not enough IRQ status\n");
    }
    if (data[2]&AXP192_IRQ_3_INTERNAL_OVER) {
        AXP_DBG("AXP192 internal over temperature IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_UNDER_VOLTAGE) {
        AXP_DBG("APS under voltage IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_VBUS_SESSION_END) {
        AXP_DBG("VBUS Session End IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_VBUS_SESSION_AB) {
        AXP_DBG("VBUS Session A/B IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_VBUS_INVLAID) {
        AXP_DBG("VBUS invalid IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_VBUS_VALID) {
        AXP_DBG("VBUS valid IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_POWER_OFF) {
        AXP_DBG("Power off by N_OE IRQ status\n");
    }
    if (data[3]&AXP192_IRQ_4_POWER_ON) {
        AXP_DBG("Power on by N_OE IRQ status\n");
    }

  ret:
    // clear
    apx192_clear_status(regmap);
    enable_irq(axp_irq->irq);
}

static irqreturn_t axp192_interrupt(int irqno, void *param)
{
    struct axp192_irq*    axp_irq = (struct axp192_irq*)param;
    AXP_DBG("%s irqno:%d\n", __func__, irqno);

    // In AXP192's mode A, IRQ/WAKEUP pin(48) acts as the level-triggered IRQ status indicator.
    // When an interrupt occurs, its output is pulled low to notify HOST an interrupt.
    // So it must be disabled, until the interrupt status registers are cleared.
    disable_irq_nosync(irqno);  // == axp_irq->irq
    queue_work(axp_irq->work_que, &axp_irq->work);
    return IRQ_HANDLED;
}

#if defined(AXP192_REPORT_POWER_KEY)
static void axp192_timer_func(unsigned long data)
{
    struct axp192_irq*  axp_irq = (struct axp192_irq*)data;
    AXP_DBG("%s\n", __func__);

    input_report_key(axp_irq->input_dev, KEY_POWER, 0);
    //input_event(axp_irq->input_dev, EV_KEY, KEY_POWER, 0);
    input_event(axp_irq->input_dev, EV_SYN, 0, 0);
}
#endif

  
static int axp192_irq_probe(struct platform_device *pdev)
{
    struct axp192_core*   core = dev_get_drvdata(pdev->dev.parent);
    struct device_node*   of_node = pdev->dev.of_node;
    struct axp192_irq*    axp_irq;
    struct regmap*        regmap = core->regmap;
    unsigned long const   irqflags = IRQ_TYPE_EDGE_FALLING|IRQF_DISABLED;
    int                   rc = -ENOMEM;
    //char const*           msg = "";
    int                   line;
    int                   gpio, irq;

    // axp_irq <--
    axp_irq = AXP_CALL(devm_kzalloc, &pdev->dev, sizeof(*axp_irq), GFP_KERNEL);
    if ( !axp_irq ) {
        goto ret;
    }
    axp_irq->core = core;
    axp_irq->dev = &pdev->dev;
    if ( ( axp_irq->work_que = AXP_CALL(create_singlethread_workqueue,"axp192_wq") ) == NULL ) {;
        goto ret;
    }

    platform_set_drvdata(pdev, axp_irq);

    // axp_irq->irq <-- setup irq
    gpio = rc = of_get_named_gpio(of_node, "axp,irq-gpio", 0 );
    if ( ! AXP_CALL(gpio_is_valid, gpio)  ||
         ( rc = AXP_CALL(devm_gpio_request, &pdev->dev, gpio, "axp192 irq") ) < 0 ||
         ( rc = AXP_CALL(gpio_direction_input, gpio) ) < 0 ||
         ( rc = irq = AXP_CALL(gpio_to_irq, gpio) ) < 0 ||
         ( rc = AXP_CALL(devm_request_irq, &pdev->dev, irq, axp192_interrupt, irqflags, "axp192_irq", axp_irq) ) < 0
        ) {
        goto ret;
    }
    axp_irq->irq = irq;


#if defined(AXP192_REPORT_POWER_KEY)
    // register input device for power key.
    if ( ( axp_irq->input_dev = AXP_CALL(devm_input_allocate_device,&pdev->dev) ) == NULL) {
        rc = -ENOMEM;
        goto ret;
    }

    axp_irq->input_dev->evbit[0] = BIT(EV_KEY);
    axp_irq->input_dev->name = "axp192 power-key";
    set_bit(KEY_POWER & KEY_MAX, axp_irq->input_dev->keybit);
    if ( ( rc = AXP_CALL(input_register_device, axp_irq->input_dev) ) != 0 ) {
        goto ret;
    }

    axp_irq->timer.data = (unsigned long)axp_irq;
    axp_irq->timer.function = axp192_timer_func;
    init_timer(&axp_irq->timer);
#endif

    INIT_WORK(&axp_irq->work, axp192_work_func);

    // clear irq status
    apx192_clear_status(regmap);

    // irq enable
    apx192_irq_enable(regmap);

  ret:
    if ( rc ) {
        if (axp_irq && axp_irq->work_que)
            destroy_workqueue(axp_irq->work_que);

        AXP_ERR("rc:%d line:%d\n", rc, line);
    } 
    AXP_DBG("%s rc:%d gpio:%d irq:%d\n", __func__, rc, gpio, irq);
    return rc;
}

static int axp192_irq_remove(struct platform_device *pdev)
{
    struct axp192_irq*  axp_irq = platform_get_drvdata(pdev);

    apx192_irq_disable( axp192_regmap( axp_irq ) );

    // devm_request_irq <--> devm_free_irq
    //      request_irq <-->      free_irq
    if ( axp_irq->irq >= 0 )
        devm_free_irq(&pdev->dev, axp_irq->irq, axp_irq);
    if (axp_irq->work_que)
        destroy_workqueue(axp_irq->work_que);

#if defined(AXP192_REPORT_POWER_KEY)
    del_timer(&axp_irq->timer);
#endif
  
    return 0;
}

static int axp192_irq_suspend(struct platform_device *pdev, pm_message_t state)
{
    struct axp192_irq*  axp_irq = platform_get_drvdata(pdev);
    struct regmap*      regmap = axp192_regmap( axp_irq );
    int                 rc;

    disable_irq(axp_irq->irq);

    // clear irq status
    apx192_clear_status(regmap);

    rc = cancel_work_sync(&axp_irq->work);
    flush_workqueue(axp_irq->work_que);

    // rc == 0 or 1.
		// rc==1 means that the work was pending and canceled, therefore we need to enable
		// IRQ here to balance the disable_irq() done in the interrupt handler.
    // ( IRQ is disabled at axp192_interrupt(), and to be enabled at axp192_work_func(),
    //   but when rc==1 axp192_work_func() was cancled, and IRQ was not enabled there. )
    if ( rc )
        enable_irq(axp_irq->irq);

    axp_irq->suspend = 1;

    AXP_DBG( "axp192_irq_suspend\n" );
    return 0;
}

static int axp192_irq_resume(struct platform_device *pdev)
{
    struct axp192_irq*  axp_irq = platform_get_drvdata(pdev);

#if defined(AXP192_REPORT_POWER_KEY)
    queue_work(axp_irq->work_que, &axp_irq->work);  // irq is to be enabled here
#else
    struct regmap*      regmap = axp192_regmap( axp_irq );

    // clear irq status and enable
    apx192_clear_status(regmap);
    enable_irq(axp_irq->irq);
#endif

#if 0
    temp = AXP192_READ(axp192_regmap(axp_irq), AXP192_POWER_STS_REG);
    if (temp & 0x80)
        axp_irq->acin_det = 1;
    else
        axp_irq->acin_det = 0;
#endif

    axp_irq->suspend = 0;

    AXP_DBG( "axp192_irq_resume\n" );
    return 0;
}


static struct of_device_id axp192_match_table[] = {
    { .compatible = AXP192_OF_COMATIBLE(irq), },
    {},
};

static struct platform_driver axp192_irq_driver = {
    .driver = {
        .name = AXP192_DEV_NAME "-irq",
        .owner = THIS_MODULE,
        .of_match_table = axp192_match_table,
    },
    .probe = axp192_irq_probe,
    .remove = axp192_irq_remove,
    .suspend = axp192_irq_suspend,
    .resume = axp192_irq_resume,
};

static int axp192_irq_init(void)
{
    AXP_DBG("axp192_irq_init\n");

    return platform_driver_register(&axp192_irq_driver);
}
subsys_initcall(axp192_irq_init);

static void axp192_irq_exit(void)
{
    platform_driver_unregister(&axp192_irq_driver);
}
module_exit(axp192_irq_exit);


